# PPK-Praktikum 8 : Android Studio

## Identitas

```
Nama : Novanni Indi Pradana
NIM  : 222011436
Kelas: 3SI3

```

## Deskripsi

Android Studio adalah Integrated Development Environment (IDE) resmi untuk pengembangan aplikasi Android, yang didasarkan pada IntelliJ IDEA. 

Selain sebagai editor kode dan fitur developer IntelliJ yang andal, Android Studio menawarkan banyak fitur yang meningkatkan produktivitas Anda dalam membuat aplikasi Android, seperti: 

Sistem build berbasis Gradle yang fleksibel. 
Emulator yang cepat dan kaya fitur. 
Lingkungan terpadu tempat Anda bisa mengembangkan aplikasi untuk semua perangkat Android.
Terapkan Perubahan untuk melakukan push pada perubahan kode dan resource ke aplikasi yang sedang berjalan tanpa memulai ulang aplikasi.
Template kode dan integrasi GitHub untuk membantu Anda membuat fitur aplikasi umum dan mengimpor kode sampel. 
Framework dan alat pengujian yang lengkap. 
Alat lint untuk merekam performa, kegunaan, kompatibilitas versi, dan masalah lainnya. 
Dukungan C++ dan NDK. 
Dukungan bawaan untuk Google Cloud Platform, yang memudahkan integrasi Google Cloud Messaging dan App Engine. 


## Kegiatan Praktikum

## 1. Virtual Devices
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-8/-/raw/main/screenshot/Screenshot%20(1).png)
## 2. Tampilan Virtual Devices
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-8/-/raw/main/screenshot/Screenshot%20(2).png)
## 3. New Project Empty Activity
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-8/-/raw/main/screenshot/Screenshot%20(3).png)
## 4. Setting New Project
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-8/-/raw/main/screenshot/Screenshot%20(4).png)
## 5. Tampilan Project
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-8/-/raw/main/screenshot/Screenshot%20(5).png)
## 5. Running Project on virtual Device
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-8/-/raw/main/screenshot/Screenshot%20(6).png)
## 5. Running Project on Personal Device
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-8/-/raw/main/screenshot/redmi_note_5_hello_world.jpeg)
